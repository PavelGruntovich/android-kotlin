package com.example.android.marsrealestate.bearing.domain





data class Bearing(val model: String,
                   val manufacturer: String,
                   val url: String,
                   val country: String,
                   val size: String,
                   val exist: String)


