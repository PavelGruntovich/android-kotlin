package com.example.android.marsrealestate.vmc

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class VmcDetailViewModel(vmcProperty: VmcProperty, app: Application) : AndroidViewModel(app) {

    val selectedProperty: LiveData<VmcProperty>  get() = _selectedProperty
    private val _selectedProperty = MutableLiveData<VmcProperty>()


    init {
        _selectedProperty.value = vmcProperty
    }


}