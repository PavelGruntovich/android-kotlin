package com.example.android.marsrealestate.lathe

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewItemLatheBinding


class LathePhotoGridAdapter(private val onClickListenerLathe: OnClickListenerLathe ) : ListAdapter<Lathe, LathePhotoGridAdapter.LatheviewHolder>(DiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LatheviewHolder {
        return LatheviewHolder(GridViewItemLatheBinding.inflate(LayoutInflater.from(parent.context)))
    }


    override fun onBindViewHolder(holder: LatheviewHolder, position: Int) {
        val lathe = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerLathe.onClick(lathe)
        }
        holder.bind(lathe)
    }


    companion object DiffCallback : DiffUtil.ItemCallback<Lathe>() {
        override fun areItemsTheSame(oldItem: Lathe, newItem: Lathe): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Lathe, newItem: Lathe): Boolean {
            return oldItem.id == newItem.id
        }
    }


    class LatheviewHolder(private var binding: GridViewItemLatheBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(lathe: Lathe) {
            binding.property = lathe
            binding.executePendingBindings()
        }
    }

    class OnClickListenerLathe(val clickListener: (lathe: Lathe) -> Unit) {
        fun onClick(lathe: Lathe) = clickListener(lathe)
    }

}