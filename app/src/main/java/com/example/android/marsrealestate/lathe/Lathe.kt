package com.example.android.marsrealestate.lathe

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Lathe(
        val id: String,
        val model: String,
        val producer: String,
        @Json(name = "video1") val img: String ) : Parcelable {


}