package com.example.android.marsrealestate.vmc

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


enum class VmcApiStatus { LOADING, ERROR, DONE }

class VmcViewModel : ViewModel() {


    val response: LiveData<String> get() = _response
    private val _response = MutableLiveData<String>()

    val properties: LiveData<List<VmcProperty>> get() = _properties
    private val _properties = MutableLiveData<List<VmcProperty>>()

    val status: LiveData<VmcApiStatus> get() = _status
    private val _status = MutableLiveData<VmcApiStatus>()

    val navigateToSelectedVmc: LiveData<VmcProperty>  get() = _navigateToSelectedVmc
    private val _navigateToSelectedVmc = MutableLiveData<VmcProperty>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        getVmcProperties(VmcApiFilter.AXES_ALL)
    }


    private fun getVmcProperties(filter: VmcApiFilter) {

        Log.d("VmcViewModel dm->", "- getVmcProperties")

        coroutineScope.launch {
            var getPropertiesDeferred = VmcApi.retrofitService.getProperties(filter.value)
            try {
                _status.value = VmcApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = VmcApiStatus.DONE
                _properties.value = listResult
            } catch (e: Exception) {
                _status.value = VmcApiStatus.ERROR
                Log.d("Error dm - > ", "I think and shure you do not have and internet connection")
                _properties.value = ArrayList()
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun displayOneVmc(vmcProperty: VmcProperty) {
        _navigateToSelectedVmc.value = vmcProperty
    }

    fun displayOneVmcComplete() {
        _navigateToSelectedVmc.value = null
    }


    fun updateFilter(filter: VmcApiFilter) {
        getVmcProperties(filter)
    }

}