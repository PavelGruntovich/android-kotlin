package com.example.android.marsrealestate.lathe

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


enum class LatheApiStatus {
    LOADING, ERROR, DONE
}

class LatheViewModel : ViewModel() {


    val response: LiveData<String> get() = _response
    private val _response = MutableLiveData<String>()

    val properties: LiveData<List<Lathe>> get() = _properties
    private val _properties = MutableLiveData<List<Lathe>>()

    val status: LiveData<LatheApiStatus> get() = _status
    private val _status = MutableLiveData<LatheApiStatus>()

    val navigateToSelectedLathe: LiveData<Lathe>  get() = _navigateToSelectedLathe
    private val _navigateToSelectedLathe = MutableLiveData<Lathe>()

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
//        getLatheProperties(LatheApiFilter.AXES_ALL)
        getLatheProperties()
    }


//    private fun getLatheProperties(filter: LatheApiFilter) {
    private fun getLatheProperties( ) {

    Log.d("LatheViewModel dm->", "- getLatheProperties")
    _response.value = "Set response Lathe"

        coroutineScope.launch {
            var getPropertiesDeferred = LatheApi.retrofitService.getProperties( )
//            var getPropertiesDeferred = LatheApi.retrofitService.getProperties(filter.value)
            try {
                _status.value = LatheApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = LatheApiStatus.DONE
                _properties.value = listResult
            } catch (e: Exception) {
                _status.value = LatheApiStatus.ERROR
                Log.d("Error dm - > ", "I think and shure you do not have and internet connection")
                _properties.value = ArrayList()
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()  // !!!
    }

    fun displayOneLathe(lathe: Lathe) {
        _navigateToSelectedLathe.value = lathe
    }

    fun displayOneLatheComplete() {
        _navigateToSelectedLathe.value = null
    }


//    fun updateFilter(filter: LatheApiFilter) {
//        getLatheProperties(filter)
//    }

}